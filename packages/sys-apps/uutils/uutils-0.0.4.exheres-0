# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo github [ user=${PN} project=coreutils tag=${PV} ] alternatives

SUMMARY="Cross-platform Rust rewrite of the GNU coreutils"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

RESTRICT="test"

DEPENDENCIES="
    build:
        dev-python/Sphinx
"

DEFAULT_SRC_COMPILE_PARAMS=(
    PROFILE=release
)

DEFAULT_SRC_INSTALL_PARAMS=(
    PROFILE=release
    PREFIX=/usr/$(exhost --target)
    MANDIR=/usr/share/man/man1
    PROG_PREFIX=uu_
)

src_unpack() {
    cargo_src_unpack
    edo cd "${WORK}"
    esandbox disable_net
    ecargo update -p onig
    esandbox enable_net
    ecargo_fetch
}

src_compile() {
    default
}

src_install() {
    default

    # sys-apps/shadow installs the same binary and manpage
    # since shadow deals with logins and such anyway, it is preferred
    edo rm "${IMAGE}"/usr/$(exhost --target)/bin/uu_groups

    local coreutils_alternatives=()
    for prog in "${IMAGE}"/usr/$(exhost --target)/bin/*;do
        prog=$(basename "${prog}")
        prog=${prog/uu_}
        case "${prog}" in
            hostname|uptime) ;;
            arch)
                coreutils_alternatives+=(
                    /usr/$(exhost --target)/bin/${prog} uu_${prog}
                    /usr/share/man/man1/${prog}.1       uu_${prog}.1
                )
                ;;
            *)
                coreutils_alternatives+=(
                    /usr/$(exhost --target)/bin/${prog} uu_${prog}
                )
                ;;
        esac
    done
    alternatives_for coreutils uutils 10 "${coreutils_alternatives[@]}"
    alternatives_for hostname  uutils 10 \
        /usr/$(exhost --target)/bin/hostname uu_hostname
    alternatives_for uptime    uutils 10 \
        /usr/$(exhost --target)/bin/uptime uu_uptime
}
